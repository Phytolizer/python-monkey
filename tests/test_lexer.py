__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"


import pytest

from monkey.lexer import Lexer
from monkey.token import TokenKind


@pytest.fixture(scope="module")
def next_token_fixture():
    return Lexer(
        """
        let five = 5;
        let ten = 10;

        let add = fn(x, y) {
            x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;

        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;
        "foobar"
        "foo bar"
        [1, 2];
        {"foo": "bar"}
        """
    )


@pytest.mark.parametrize(
    "kind,literal",
    (
        (TokenKind.LET, "let"),
        (TokenKind.IDENT, "five"),
        (TokenKind.ASSIGN, "="),
        (TokenKind.INT, "5"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.LET, "let"),
        (TokenKind.IDENT, "ten"),
        (TokenKind.ASSIGN, "="),
        (TokenKind.INT, "10"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.LET, "let"),
        (TokenKind.IDENT, "add"),
        (TokenKind.ASSIGN, "="),
        (TokenKind.FUNCTION, "fn"),
        (TokenKind.LPAREN, "("),
        (TokenKind.IDENT, "x"),
        (TokenKind.COMMA, ","),
        (TokenKind.IDENT, "y"),
        (TokenKind.RPAREN, ")"),
        (TokenKind.LBRACE, "{"),
        (TokenKind.IDENT, "x"),
        (TokenKind.PLUS, "+"),
        (TokenKind.IDENT, "y"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.RBRACE, "}"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.LET, "let"),
        (TokenKind.IDENT, "result"),
        (TokenKind.ASSIGN, "="),
        (TokenKind.IDENT, "add"),
        (TokenKind.LPAREN, "("),
        (TokenKind.IDENT, "five"),
        (TokenKind.COMMA, ","),
        (TokenKind.IDENT, "ten"),
        (TokenKind.RPAREN, ")"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.BANG, "!"),
        (TokenKind.MINUS, "-"),
        (TokenKind.SLASH, "/"),
        (TokenKind.ASTERISK, "*"),
        (TokenKind.INT, "5"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.INT, "5"),
        (TokenKind.LT, "<"),
        (TokenKind.INT, "10"),
        (TokenKind.GT, ">"),
        (TokenKind.INT, "5"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.IF, "if"),
        (TokenKind.LPAREN, "("),
        (TokenKind.INT, "5"),
        (TokenKind.LT, "<"),
        (TokenKind.INT, "10"),
        (TokenKind.RPAREN, ")"),
        (TokenKind.LBRACE, "{"),
        (TokenKind.RETURN, "return"),
        (TokenKind.TRUE, "true"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.RBRACE, "}"),
        (TokenKind.ELSE, "else"),
        (TokenKind.LBRACE, "{"),
        (TokenKind.RETURN, "return"),
        (TokenKind.FALSE, "false"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.RBRACE, "}"),
        (TokenKind.INT, "10"),
        (TokenKind.EQ, "=="),
        (TokenKind.INT, "10"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.INT, "10"),
        (TokenKind.NOT_EQ, "!="),
        (TokenKind.INT, "9"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.STRING, "foobar"),
        (TokenKind.STRING, "foo bar"),
        (TokenKind.LBRACKET, "["),
        (TokenKind.INT, "1"),
        (TokenKind.COMMA, ","),
        (TokenKind.INT, "2"),
        (TokenKind.RBRACKET, "]"),
        (TokenKind.SEMICOLON, ";"),
        (TokenKind.LBRACE, "{"),
        (TokenKind.STRING, "foo"),
        (TokenKind.COLON, ":"),
        (TokenKind.STRING, "bar"),
        (TokenKind.RBRACE, "}"),
        (TokenKind.EOF, ""),
    ),
)
def test_next_token(next_token_fixture, kind, literal):
    """Verify that Lexer.next_token is working correctly."""
    token = next_token_fixture.next_token()
    assert token.kind == kind
    assert token.literal == literal
