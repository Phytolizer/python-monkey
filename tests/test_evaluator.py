import pytest

from monkey.evaluator import FALSE, TRUE, Evaluator
from monkey.lexer import Lexer
from monkey.object import (
    Array,
    Boolean,
    Environment,
    Error,
    Function,
    Hash,
    Integer,
    Null,
    String,
)
from monkey.parser import Parser


def do_eval(text):
    lexer = Lexer(text)
    parser = Parser(lexer)
    program = parser.parse_program()
    env = Environment()
    return Evaluator().eval(program, env)


def check_integer_object(actual, expected):
    assert isinstance(actual, Integer)
    assert actual.value == expected


def check_boolean_object(actual, expected):
    assert isinstance(actual, Boolean)
    assert actual.value == expected


def check_null_object(actual):
    assert isinstance(actual, Null)


def check_error_object(actual, expected):
    assert isinstance(actual, Error)
    assert actual.message == expected


def check_object(actual, expected):
    if isinstance(expected, int):
        check_integer_object(actual, expected)
    elif isinstance(expected, bool):
        check_boolean_object(actual, expected)
    elif expected is None:
        check_null_object(actual)
    else:
        pytest.fail(f"Unexpected object {expected}")


@pytest.mark.parametrize(
    "text,expected",
    (
        ("5", 5),
        ("10", 10),
        ("-5", -5),
        ("-10", -10),
        ("5 + 5 + 5 + 5 - 10", 10),
        ("2 * 2 * 2 * 2 * 2", 32),
        ("-50 + 100 + -50", 0),
        ("5 * 2 + 10", 20),
        ("5 + 2 * 10", 25),
        ("20 + 2 * -10", 0),
        ("50 / 2 * 2 + 10", 60),
        ("2 * (5 + 10)", 30),
        ("3 * 3 * 3 + 10", 37),
        ("3 * (3 * 3) + 10", 37),
        ("(5 + 10 * 2 + 15 / 3) * 2 + -10", 50),
    ),
)
def test_eval_integer_expression(text, expected):
    evaluated = do_eval(text)
    check_integer_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        ("true", True),
        ("false", False),
        ("1 < 2", True),
        ("1 > 2", False),
        ("1 < 1", False),
        ("1 > 1", False),
        ("1 == 1", True),
        ("1 != 1", False),
        ("1 == 2", False),
        ("1 != 2", True),
        ("true == true", True),
        ("false == false", True),
        ("true == false", False),
        ("true != false", True),
        ("false != true", True),
        ("(1 < 2) == true", True),
        ("(1 < 2) == false", False),
        ("(1 > 2) == true", False),
        ("(1 > 2) == false", True),
    ),
)
def test_eval_boolean_expression(text, expected):
    evaluated = do_eval(text)
    check_boolean_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        ("!true", False),
        ("!false", True),
        ("!5", False),
        ("!!true", True),
        ("!!false", False),
        ("!!5", True),
    ),
)
def test_bang_operator(text, expected):
    evaluated = do_eval(text)
    check_boolean_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        ("if (true) { 10 }", 10),
        ("if (false) { 10 }", None),
        ("if (1) { 10 }", 10),
        ("if (1 < 2) { 10 }", 10),
        ("if (1 > 2) { 10 }", None),
        ("if (1 > 2) { 10 } else { 20 }", 20),
        ("if (1 < 2) { 10 } else { 20 }", 10),
    ),
)
def test_if_else_expressions(text, expected):
    evaluated = do_eval(text)
    check_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        ("return 10;", 10),
        ("return 10; 9;", 10),
        ("return 2 * 5; 9;", 10),
        ("9; return 2 * 5; 9;", 10),
        (
            """
            if (10 > 1) {
                if (10 > 1) {
                    return 10;
                }

                return 1;
            }
            """,
            10,
        ),
    ),
)
def test_return_statements(text, expected):
    evaluated = do_eval(text)
    check_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        (
            "5 + true;",
            "type mismatch: INTEGER + BOOLEAN",
        ),
        (
            "5 + true; 5;",
            "type mismatch: INTEGER + BOOLEAN",
        ),
        (
            "-true",
            "unknown operator: -BOOLEAN",
        ),
        (
            "true + false;",
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        (
            "5; true + false; 5",
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        (
            "if (10 > 1) { true + false; }",
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        (
            """
            if (10 > 1) {
                if (10 > 1) {
                    return true + false;
                }

                return 1;
            }
            """,
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        (
            "foobar",
            "identifier not found: foobar",
        ),
        (
            '"Hello" - "World"',
            "unknown operator: STRING - STRING",
        ),
        (
            '{"name": "Monkey"}[fn(x) { x }];',
            "unusable as hash key: FUNCTION",
        ),
    ),
)
def test_error_handling(text, expected):
    evaluated = do_eval(text)
    check_error_object(evaluated, expected)


@pytest.mark.parametrize(
    "text,expected",
    (
        ("let a = 5; a;", 5),
        ("let a = 5 * 5; a;", 25),
        ("let a = 5; let b = a; b;", 5),
        ("let a = 5; let b = a; let c = a + b + 5; c;", 15),
    ),
)
def test_let_statements(text, expected):
    check_object(do_eval(text), expected)


def test_function_object():
    text = "fn(x) { x + 2; }"
    evaluated = do_eval(text)
    assert isinstance(evaluated, Function)
    assert len(evaluated.parameters) == 1
    assert evaluated.parameters[0].string() == "x"
    assert evaluated.body.string() == "(x + 2)"


@pytest.mark.parametrize(
    "text,expected",
    (
        ("let identity = fn(x) { x; }; identity(5);", 5),
        ("let identity = fn(x) { return x; }; identity(5);", 5),
        ("let double = fn(x) { x * 2; }; double(5);", 10),
        ("let add = fn(x, y) { x + y; }; add(5, 5);", 10),
        ("let add = fn(x, y) { x + y; }; add(5 + 5, add(5, 5));", 20),
        ("fn(x) { x; }(5)", 5),
    ),
)
def test_function_application(text, expected):
    check_object(do_eval(text), expected)


def test_closures():
    text = """
        let newAdder = fn(x) {
            fn(y) { x + y };
        };

        let addTwo = newAdder(2);
        addTwo(2);
    """
    check_object(do_eval(text), 4)


def test_string_literal():
    text = '"Hello World!"'
    evaluated = do_eval(text)
    assert isinstance(evaluated, String)
    assert evaluated.value == "Hello World!"


def test_string_concatenation():
    text = '"Hello" + " " + "World!"'
    evaluated = do_eval(text)
    assert isinstance(evaluated, String)
    assert evaluated.value == "Hello World!"


@pytest.mark.parametrize(
    "text,expected",
    (
        ('len("")', 0),
        ('len("four")', 4),
        ('len("hello world")', 11),
        ("len(1)", "argument to `len` not supported, got INTEGER"),
        ('len("one", "two")', "wrong number of arguments. got=2, want=1"),
        (
            """
            let reduce = fn(arr, initial, f) {
                let iter = fn(arr, result) {
                    if (len(arr) == 0) {
                        result
                    } else {
                        iter(rest(arr), f(result, first(arr)));
                    }
                };

                iter(arr, initial);
            };
            let sum = fn(arr) {
                reduce(arr, 0, fn(initial, el) { initial + el });
            };
            sum([1, 2, 3, 4, 5]);
            """,
            15,
        ),
    ),
)
def test_builtin_functions(text, expected):
    if isinstance(expected, str):
        check_error_object(do_eval(text), expected)
    else:
        check_object(do_eval(text), expected)


def test_array_literals():
    text = "[1, 2 * 2, 3 + 3]"
    exp = do_eval(text)
    assert isinstance(exp, Array)
    assert len(exp.elements) == 3
    check_integer_object(exp.elements[0], 1)
    check_integer_object(exp.elements[1], 4)
    check_integer_object(exp.elements[2], 6)


@pytest.mark.parametrize(
    "text,expected",
    (
        (
            "[1, 2, 3][0]",
            1,
        ),
        (
            "[1, 2, 3][1]",
            2,
        ),
        (
            "[1, 2, 3][2]",
            3,
        ),
        (
            "let i = 0; [1][i];",
            1,
        ),
        (
            "[1, 2, 3][1 + 1];",
            3,
        ),
        (
            "let myArray = [1, 2, 3]; myArray[2];",
            3,
        ),
        (
            "let myArray = [1, 2, 3]; myArray[0] + myArray[1] + myArray[2];",
            6,
        ),
        (
            "let myArray = [1, 2, 3]; let i = myArray[0]; myArray[i]",
            2,
        ),
        (
            "[1, 2, 3][3]",
            None,
        ),
        (
            "[1, 2, 3][-1]",
            None,
        ),
    ),
)
def test_array_index_expressions(text, expected):
    check_object(do_eval(text), expected)


def test_hash_literals():
    text = """
        let two = "two";
        {
            "one": 10 - 9,
            two: 1 + 1,
            "thr" + "ee": 6 / 2,
            4: 4,
            true: 5,
            false: 6
        }
        """
    evaluated = do_eval(text)
    assert isinstance(evaluated, Hash)

    expected = {
        String("one").hash_key(): 1,
        String("two").hash_key(): 2,
        String("three").hash_key(): 3,
        Integer(4).hash_key(): 4,
        TRUE.hash_key(): 5,
        FALSE.hash_key(): 6,
    }
    assert len(evaluated.pairs) == len(expected)
    for (expected_key, expected_value) in expected.items():
        pair = evaluated.pairs[expected_key]
        check_integer_object(pair.value, expected_value)


@pytest.mark.parametrize(
    "text,expected",
    (
        (
            '{"foo": 5}["foo"]',
            5,
        ),
        (
            '{"foo": 5}["bar"]',
            None,
        ),
        (
            'let key = "foo"; {"foo": 5}[key]',
            5,
        ),
        (
            '{}["foo"]',
            None,
        ),
        (
            "{5: 5}[5]",
            5,
        ),
        (
            "{true: 5}[true]",
            5,
        ),
        (
            "{false: 5}[false]",
            5,
        ),
    ),
)
def test_hash_index_expressions(text, expected):
    evaluated = do_eval(text)
    check_object(evaluated, expected)
