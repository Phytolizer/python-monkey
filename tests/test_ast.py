__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"


from monkey.ast import Identifier, LetStatement, Program
from monkey.token import Token, TokenKind


def test_string():
    program = Program(
        statements=(
            LetStatement(
                token=Token(TokenKind.LET, "let"),
                name=Identifier(
                    token=Token(TokenKind.IDENT, "myVar"),
                    value="myVar",
                ),
                value=Identifier(
                    token=Token(TokenKind.IDENT, "anotherVar"),
                    value="anotherVar",
                ),
            ),
        ),
    )

    assert program.string() == "let myVar = anotherVar;"
