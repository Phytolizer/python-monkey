__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"


import pytest

from monkey.ast import (
    ArrayLiteral,
    Boolean,
    CallExpression,
    ExpressionStatement,
    FunctionLiteral,
    HashLiteral,
    Identifier,
    IfExpression,
    IndexExpression,
    InfixExpression,
    IntegerLiteral,
    LetStatement,
    PrefixExpression,
    ReturnStatement,
    StringLiteral,
)
from monkey.lexer import Lexer
from monkey.parser import Parser


def check_parser_errors(parser):
    assert parser.errors == ()


def parse_program(text):
    lexer = Lexer(text)
    parser = Parser(lexer)
    program = parser.parse_program()
    assert program is not None
    check_parser_errors(parser)
    return program


def parse_expression(text):
    program = parse_program(text)
    assert len(program.statements) == 1
    stmt = program.statements[0]
    assert isinstance(stmt, ExpressionStatement)
    return stmt.expression


def check_let_statement(stmt, name, value):
    assert stmt.token_literal == "let"
    assert isinstance(stmt, LetStatement)
    assert stmt.name.value == name
    assert stmt.name.token_literal == name
    check_literal_expression(stmt.value, value)
    assert stmt.string() == f"let {name} = {literal(value)};"


def check_integer_literal(il, value):
    assert isinstance(il, IntegerLiteral)
    assert il.value == value
    assert il.token_literal == str(value)


def check_identifier(ident, value):
    assert isinstance(ident, Identifier)
    assert ident.value == value
    assert ident.token_literal == value
    assert ident.string() == value


def check_boolean_literal(boolean, value):
    assert isinstance(boolean, Boolean)
    assert boolean.value == value
    assert boolean.token_literal == literal(value)
    assert boolean.string() == literal(value)


def check_literal_expression(exp, expected):
    if isinstance(expected, bool):
        check_boolean_literal(exp, expected)
    elif isinstance(expected, int):
        check_integer_literal(exp, expected)
    elif isinstance(expected, str):
        check_identifier(exp, expected)
    else:
        pytest.xfail(f"Unhandled expression type {expected}.")


def literal(value):
    if isinstance(value, bool):
        return "true" if value else "false"
    if isinstance(value, int):
        return str(value)
    if isinstance(value, str):
        return value
    pytest.xfail(f"Unhandled stringof type {value}.")


def check_prefix_expression(exp, operator, right):
    assert isinstance(exp, PrefixExpression)
    assert exp.operator == operator
    check_literal_expression(exp.right, right)
    assert exp.string() == f"({operator}{literal(right)})"


def check_infix_expression(exp, left, operator, right):
    assert isinstance(exp, InfixExpression)
    check_literal_expression(exp.left, left)
    assert exp.operator == operator
    check_literal_expression(exp.right, right)
    assert exp.string() == f"({literal(left)} {operator} {literal(right)})"


def let_statements_data():
    text = """
        let x = 5;
        let y = true;
        let foobar = y;
    """

    program = parse_program(text)
    assert len(program.statements) == 3
    return zip(
        program.statements,
        (
            ("x", 5),
            ("y", True),
            ("foobar", "y"),
        ),
    )


@pytest.mark.parametrize(
    "stmt,expected",
    let_statements_data(),
)
def test_let_statements(stmt, expected):
    (name, value) = expected
    check_let_statement(stmt, name, value)


def return_statements_data():
    text = """
        return 5;
        return true;
        return x;
    """

    program = parse_program(text)
    assert len(program.statements) == 3
    return zip(
        program.statements,
        (
            5,
            True,
            "x",
        ),
    )


@pytest.mark.parametrize("stmt,expected", return_statements_data())
def test_return_statements(stmt, expected):
    assert isinstance(stmt, ReturnStatement)
    assert stmt.token_literal == "return"
    check_literal_expression(stmt.return_value, expected)


def test_identifier_expression():
    text = "foobar;"
    ident = parse_expression(text)
    check_identifier(ident, "foobar")


def test_integer_literal_expression():
    text = "5;"
    literal = parse_expression(text)
    check_integer_literal(literal, 5)


@pytest.mark.parametrize(
    "text,operator,value",
    (
        ("!5;", "!", 5),
        ("-15;", "-", 15),
        ("!true;", "!", True),
        ("!false;", "!", False),
    ),
)
def test_parsing_prefix_expressions(text, operator, value):
    exp = parse_expression(text)
    check_prefix_expression(exp, operator, value)


@pytest.mark.parametrize(
    "text,left_value,operator,right_value",
    (
        ("5 + 5;", 5, "+", 5),
        ("5 - 5;", 5, "-", 5),
        ("5 * 5;", 5, "*", 5),
        ("5 / 5;", 5, "/", 5),
        ("5 > 5;", 5, ">", 5),
        ("5 < 5;", 5, "<", 5),
        ("5 == 5;", 5, "==", 5),
        ("5 != 5;", 5, "!=", 5),
        ("true == true", True, "==", True),
        ("true != false", True, "!=", False),
        ("false == false", False, "==", False),
    ),
)
def test_parsing_infix_expressions(text, left_value, operator, right_value):
    exp = parse_expression(text)
    check_infix_expression(exp, left_value, operator, right_value)


@pytest.mark.parametrize(
    "text,expected",
    (
        (
            "-a * b",
            "((-a) * b)",
        ),
        (
            "!-a",
            "(!(-a))",
        ),
        (
            "a + b + c",
            "((a + b) + c)",
        ),
        (
            "a + b - c",
            "((a + b) - c)",
        ),
        (
            "a * b * c",
            "((a * b) * c)",
        ),
        (
            "a * b / c",
            "((a * b) / c)",
        ),
        (
            "a + b / c",
            "(a + (b / c))",
        ),
        (
            "a + b * c + d / e - f",
            "(((a + (b * c)) + (d / e)) - f)",
        ),
        (
            "3 + 4; -5 * 5",
            "(3 + 4)((-5) * 5)",
        ),
        (
            "5 > 4 == 3 < 4",
            "((5 > 4) == (3 < 4))",
        ),
        (
            "5 < 4 != 3 > 4",
            "((5 < 4) != (3 > 4))",
        ),
        (
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
        ),
        (
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
        ),
        (
            "true",
            "true",
        ),
        (
            "false",
            "false",
        ),
        (
            "3 > 5 == false",
            "((3 > 5) == false)",
        ),
        (
            "3 < 5 == true",
            "((3 < 5) == true)",
        ),
        (
            "1 + (2 + 3) + 4",
            "((1 + (2 + 3)) + 4)",
        ),
        (
            "(5 + 5) * 2",
            "((5 + 5) * 2)",
        ),
        (
            "2 / (5 + 5)",
            "(2 / (5 + 5))",
        ),
        (
            "-(5 + 5)",
            "(-(5 + 5))",
        ),
        (
            "!(true == true)",
            "(!(true == true))",
        ),
        (
            "a + add(b * c) + d",
            "((a + add((b * c))) + d)",
        ),
        (
            "add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8))",
            "add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))",
        ),
        (
            "add(a + b + c * d / f + g)",
            "add((((a + b) + ((c * d) / f)) + g))",
        ),
        (
            "a * [1, 2, 3, 4][b * c] * d",
            "((a * ([1, 2, 3, 4][(b * c)])) * d)",
        ),
        (
            "add(a * b[2], b[1], 2 * [1, 2][1])",
            "add((a * (b[2])), (b[1]), (2 * ([1, 2][1])))",
        ),
    ),
)
def test_operator_precedence_parsing(text, expected):
    program = parse_program(text)
    assert program.string() == expected


@pytest.mark.parametrize("text,value", (("true", True), ("false", False)))
def test_boolean_literal_parsing(text, value):
    exp = parse_expression(text)
    check_boolean_literal(exp, value)


def test_if_expression():
    text = "if (x < y) { x }"
    exp = parse_expression(text)
    assert isinstance(exp, IfExpression)
    check_infix_expression(exp.condition, "x", "<", "y")
    assert len(exp.consequence.statements) == 1
    consequence = exp.consequence.statements[0]
    assert isinstance(consequence, ExpressionStatement)
    check_identifier(consequence.expression, "x")
    assert exp.alternative is None


def test_if_else_expression():
    text = "if (x < y) { x } else { y }"
    exp = parse_expression(text)
    assert isinstance(exp, IfExpression)
    check_infix_expression(exp.condition, "x", "<", "y")
    assert len(exp.consequence.statements) == 1
    consequence = exp.consequence.statements[0]
    assert isinstance(consequence, ExpressionStatement)
    check_identifier(consequence.expression, "x")
    assert exp.alternative is not None
    assert len(exp.alternative.statements) == 1
    alternative = exp.alternative.statements[0]
    assert isinstance(alternative, ExpressionStatement)
    check_identifier(alternative.expression, "y")


def test_function_literal_parsing():
    text = "fn(x, y) { x + y; }"
    exp = parse_expression(text)
    assert isinstance(exp, FunctionLiteral)
    assert len(exp.parameters) == 2
    check_literal_expression(exp.parameters[0], "x")
    check_literal_expression(exp.parameters[1], "y")
    assert len(exp.body.statements) == 1
    body_stmt = exp.body.statements[0]
    assert isinstance(body_stmt, ExpressionStatement)
    check_infix_expression(body_stmt.expression, "x", "+", "y")


@pytest.mark.parametrize(
    "text,params",
    (
        ("fn() {};", ()),
        ("fn(x) {};", ("x")),
        ("fn(x, y, z) {};", ("x", "y", "z")),
    ),
)
def test_function_parameters_parsing(text, params):
    exp = parse_expression(text)
    assert isinstance(exp, FunctionLiteral)
    assert len(exp.parameters) == len(params)
    for actual, expected in zip(exp.parameters, params):
        check_identifier(actual, expected)


def test_call_expression_parsing():
    text = "add(1, 2 * 3, 4 + 5)"
    exp = parse_expression(text)
    assert isinstance(exp, CallExpression)
    check_identifier(exp.function, "add")
    assert len(exp.arguments) == 3
    check_literal_expression(exp.arguments[0], 1)
    check_infix_expression(exp.arguments[1], 2, "*", 3)
    check_infix_expression(exp.arguments[2], 4, "+", 5)


def test_string_literal_expression():
    text = '"hello world";'
    exp = parse_expression(text)
    assert isinstance(exp, StringLiteral)
    assert exp.token_literal == "hello world"
    assert exp.value == "hello world"
    assert exp.string() == "hello world"


def test_parsing_array_literals():
    text = "[1, 2 * 2, 3 + 3]"
    exp = parse_expression(text)
    assert isinstance(exp, ArrayLiteral)
    assert len(exp.elements) == 3
    check_integer_literal(exp.elements[0], 1)
    check_infix_expression(exp.elements[1], 2, "*", 2)
    check_infix_expression(exp.elements[2], 3, "+", 3)


def test_parsing_index_expressions():
    text = "myArray[1 + 1]"
    exp = parse_expression(text)
    assert isinstance(exp, IndexExpression)
    check_identifier(exp.left, "myArray")
    check_infix_expression(exp.index, 1, "+", 1)


def test_parsing_hash_literals_string_keys():
    text = '{"one": 1, "two": 2, "three": 3}'
    exp = parse_expression(text)
    assert isinstance(exp, HashLiteral)
    assert len(exp.pairs) == 3
    expected = {
        "one": 1,
        "two": 2,
        "three": 3,
    }
    for ((actual_key, actual_value), (expected_key, expected_value)) in zip(
        exp.pairs.items(), expected.items()
    ):
        assert isinstance(actual_key, StringLiteral)
        assert actual_key.string() == expected_key
        check_integer_literal(actual_value, expected_value)


def test_parsing_empty_hash_literal():
    text = "{}"
    exp = parse_expression(text)
    assert isinstance(exp, HashLiteral)
    assert len(exp.pairs) == 0


def test_parsing_hash_literals_with_expressions():
    text = """
        {
            "one": 0 + 1,
            "two": 10 - 8,
            "three": 15 / 5
        }
        """
    exp = parse_expression(text)
    assert isinstance(exp, HashLiteral)
    assert len(exp.pairs) == 3
    tests = {
        "one": lambda e: check_infix_expression(e, 0, "+", 1),
        "two": lambda e: check_infix_expression(e, 10, "-", 8),
        "three": lambda e: check_infix_expression(e, 15, "/", 5),
    }
    for ((actual_key, actual_value), (expected_key, value_func)) in zip(
        exp.pairs.items(), tests.items()
    ):
        assert isinstance(actual_key, StringLiteral)
        assert actual_key.string() == expected_key
        value_func(actual_value)
