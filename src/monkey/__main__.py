__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

import sys
from getpass import getuser

from monkey import repl

user = getuser()
print(f"Hello {user}! This is the Monkey programming language!")
print("Feel free to type in commands")
repl.start(sys.stdin, sys.stdout)
