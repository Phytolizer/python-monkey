from abc import ABC, abstractmethod
from enum import Enum


class ObjectType(str, Enum):
    INTEGER = "INTEGER"
    BOOLEAN = "BOOLEAN"
    NULL = "NULL"
    RETURN_VALUE = "RETURN_VALUE"
    ERROR = "ERROR"
    FUNCTION = "FUNCTION"
    STRING = "STRING"
    BUILTIN = "BUILTIN"
    ARRAY = "ARRAY"
    HASH = "HASH"


class Object(ABC):
    @property
    @abstractmethod
    def type(self) -> ObjectType:
        pass

    @abstractmethod
    def inspect(self) -> str:
        pass

    @abstractmethod
    def __repr__(self) -> str:
        pass


class HashKey:
    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __eq__(self, other):
        return self.type == other.type and self.value == other.value

    def __hash__(self) -> int:
        return hash((self.type, self.value))

    def __repr__(self) -> str:
        return f"HashKey({self.type}, {self.value})"


class Hashable(ABC):
    @abstractmethod
    def hash_key(self) -> HashKey:
        pass


class Integer(Object, Hashable):
    def __init__(self, value):
        self.value = value

    @property
    def type(self) -> ObjectType:
        return ObjectType.INTEGER

    def inspect(self) -> str:
        return str(self.value)

    def __repr__(self) -> str:
        return f"Integer({self.inspect()})"

    def hash_key(self) -> HashKey:
        return HashKey(self.type, self.value)


class Boolean(Object, Hashable):
    def __init__(self, value):
        self.value = value

    @property
    def type(self) -> ObjectType:
        return ObjectType.BOOLEAN

    def inspect(self) -> str:
        return str(self.value).lower()

    def __repr__(self) -> str:
        return f"Boolean({self.inspect()})"

    def hash_key(self) -> HashKey:
        value = 1 if self.value else 0
        return HashKey(self.type, value)


class Null(Object):
    @property
    def type(self) -> ObjectType:
        return ObjectType.NULL

    def inspect(self) -> str:
        return "null"

    def __repr__(self) -> str:
        return "Null"


class ReturnValue(Object):
    def __init__(self, value):
        self.value = value

    @property
    def type(self) -> ObjectType:
        return ObjectType.RETURN_VALUE

    def inspect(self) -> str:
        return self.value.inspect()

    def __repr__(self) -> str:
        return f"ReturnValue({self.inspect()})"


class Error(Object):
    def __init__(self, message):
        self.message = message

    @property
    def type(self) -> ObjectType:
        return ObjectType.ERROR

    def inspect(self) -> str:
        return f"ERROR: {self.message}"

    def __repr__(self) -> str:
        return f"Error({self.inspect()})"


class Function(Object):
    def __init__(self, parameters, body, env):
        self.parameters = parameters
        self.body = body
        self.env = env

    @property
    def type(self) -> ObjectType:
        return ObjectType.FUNCTION

    def inspect(self) -> str:
        result = "fn("
        result += ", ".join(map(lambda param: param.string(), self.parameters))
        result += f") {{\n{self.body.string()}\n}}"
        return result

    def __repr__(self) -> str:
        return f"Function({self.inspect()})"


class String(Object, Hashable):
    def __init__(self, value):
        self.value = value

    @property
    def type(self) -> ObjectType:
        return ObjectType.STRING

    def inspect(self) -> str:
        return self.value

    def __repr__(self) -> str:
        return f"String({self.inspect()})"

    def hash_key(self) -> HashKey:
        h = hash(self.value)
        return HashKey(self.type, h)


class Builtin(Object):
    def __init__(self, fn):
        self.fn = fn

    @property
    def type(self) -> ObjectType:
        return ObjectType.BUILTIN

    def inspect(self) -> str:
        return "builtin function"

    def __repr__(self) -> str:
        return f"Builtin({self.fn})"


class Array(Object):
    def __init__(self, elements):
        self.elements = elements

    @property
    def type(self) -> ObjectType:
        return ObjectType.ARRAY

    def inspect(self) -> str:
        elements = ", ".join(map(lambda element: element.inspect(), self.elements))
        return f"[{elements}]"

    def __repr__(self) -> str:
        return f"Array({self.inspect()})"


class HashPair:
    def __init__(self, key, value):
        self.key = key
        self.value = value


class Hash(Object):
    def __init__(self, pairs):
        self.pairs = pairs

    @property
    def type(self) -> ObjectType:
        return ObjectType.HASH

    def inspect(self) -> str:
        pairs = ", ".join(f"{k}: {v}" for k, v in self.pairs.items())
        return f"{{{pairs}}}"

    def __repr__(self) -> str:
        return f"Hash({self.inspect()})"


class Environment:
    def __init__(self, outer=None):
        self.store = {}
        self.outer = outer

    def get(self, name):
        result = self.store.get(name)
        if result is not None:
            return result
        if self.outer is not None:
            return self.outer.get(name)
        raise KeyError(name)

    def set(self, name, val):
        self.store[name] = val
        return val
