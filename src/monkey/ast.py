__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

from abc import ABC, abstractmethod


class Node(ABC):
    """
    A generic node in the Monkey parse tree.
    """

    @property
    @abstractmethod
    def token_literal(self) -> str:
        """
        Traverse the tree preorder and return the first leaf (token)'s literal.

        This property is typically not useful beyond debugging purposes.
        """
        pass

    @abstractmethod
    def string(self) -> str:
        """
        Get a test-friendly string representation of this node.
        """
        pass

    def __str__(self) -> str:
        return self.string()

    @abstractmethod
    def __repr__(self) -> str:
        pass


class Statement(Node, ABC):
    """
    These nodes represent statements in the language.
    Statements are typically terminated by semicolons and do not
    resolve to a type, but rather impact the state of the program
    through their effects on control flow and the environment.
    """

    pass


class Expression(Node, ABC):
    """
    These nodes represent expressions in the language.
    Expressions resolve to a value (with a type), and typically do not affect
    control flow or the environment.
    """

    pass


class Program(Node):
    """
    The root node of the parse tree.
    A program consists of a sequence of statements.
    """

    def __init__(self, statements):
        self.statements = statements

    @property
    def token_literal(self) -> str:
        if len(self.statements) > 0:
            return self.statements[0].token_literal()
        return ""

    def string(self) -> str:
        return "".join(map(lambda stmt: stmt.string(), self.statements))

    def __repr__(self) -> str:
        return f"Program({self.string()})"


class LetStatement(Statement):
    """
    A let statement. Binds a name to a value.

    ```
    let x = 10;
    ```
    """

    def __init__(self, token, name, value):
        self.token = token
        self.name = name
        self.value = value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        result = f"{self.token_literal} {self.name.string()} = "
        if self.value is not None:
            result += self.value.string()
        result += ";"
        return result

    def __repr__(self) -> str:
        return f"LetStatement({self.string()})"


class Identifier(Expression):
    """
    An identifier expression. Refers to a binding in the environment.

    ```
    x
    ```
    """

    def __init__(self, token, value):
        self.token = token
        self.value = value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return self.value

    def __repr__(self) -> str:
        return f"Identifier({self.string()})"


class ReturnStatement(Statement):
    """
    A return statement. Exits a function, optionally with a value attached.

    ```
    return true;
    ```
    """

    def __init__(self, token, return_value):
        self.token = token
        self.return_value = return_value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        result = f"{self.token_literal} "
        if self.return_value is not None:
            result += self.return_value.string()
        result += ";"
        return result

    def __repr__(self) -> str:
        return f"ReturnStatement({self.string()})"


class ExpressionStatement(Statement):
    """
    An expression statement. Consists only of an expression.

    ```
    x + y;
    ```
    """

    def __init__(self, token, expression):
        self.token = token
        self.expression = expression

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        if self.expression is not None:
            return self.expression.string()
        return ""

    def __repr__(self) -> str:
        return f"ExpressionStatement({self.string()})"


class IntegerLiteral(Expression):
    """
    An integer literal. Consists of a literal number.

    ```
    42
    ```
    """

    def __init__(self, token, value):
        self.token = token
        self.value = value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return self.token.literal

    def __repr__(self) -> str:
        return f"IntegerLiteral({self.string()})"


class PrefixExpression(Expression):
    """
    A prefix expression. Consists of an operator followed by an expression.

    ```
    -x
    ```
    """

    def __init__(self, token, operator, right):
        self.token = token
        self.operator = operator
        self.right = right

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return f"({self.operator}{self.right.string()})"

    def __repr__(self) -> str:
        return f"PrefixExpression({self.string()})"


class InfixExpression(Expression):
    """
    An infix expression. Consists of two expressions
    and an operator in the middle.
    """

    def __init__(self, token, left, operator, right):
        self.token = token
        self.left = left
        self.operator = operator
        self.right = right

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return f"({self.left.string()} {self.operator} {self.right.string()})"

    def __repr__(self) -> str:
        return f"InfixExpression({self.string()})"


class Boolean(Expression):
    """
    A boolean literal.

    ```
    true
    ```
    """

    def __init__(self, token, value):
        self.token = token
        self.value = value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return self.token.literal

    def __repr__(self) -> str:
        return f"Boolean({self.token.literal})"


class IfExpression(Expression):
    """
    An if expression. Consists of a boolean condition followed by
    a block statement. Optionally can also contain an else clause
    which will be evaluated if the condition is not met.

    ```
    if x {
        y
    } else {
        z
    }
    ```
    """

    def __init__(self, token, condition, consequence, alternative):
        self.token = token
        self.condition = condition
        self.consequence = consequence
        self.alternative = alternative

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        result = f"if{self.condition.string()} {self.consequence.string()}"
        if self.alternative is not None:
            result += f" else {self.alternative.string()}"
        return result

    def __repr__(self) -> str:
        return f"IfExpression({self.string()})"


class BlockStatement(Statement):
    """
    A block statement.
    Consists of a sequence of statements.
    Useful for grouping a sequence of statements into a single statement.

    ```
    {
        let x = 1;
        let y = 2;
    }
    ```
    """

    def __init__(self, token, statements):
        self.token = token
        self.statements = statements

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return "".join(map(lambda stmt: stmt.string(), self.statements))

    def __repr__(self) -> str:
        return f"BlockStatement({self.string()})"


class FunctionLiteral(Expression):
    """
    A function expression. Consists of a set of arguments and
    a block statement, which is the body of the function.

    ```
    fn(x) { x }
    ```
    """

    def __init__(self, token, parameters, body):
        self.token = token
        self.parameters = parameters
        self.body = body

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        params = ", ".join(map(lambda param: param.string(), self.parameters))
        return f"{self.token_literal}({params}) {self.body.string()}"

    def __repr__(self) -> str:
        return f"FunctionLiteral({self.string()})"


class CallExpression(Expression):
    """
    A call to a function. Consists of at least two expressions.
    One should resolve to a function, the rest are arguments to the function.

    ```
    x(y, z)
    ```
    """

    def __init__(self, token, function, arguments):
        self.token = token
        self.function = function
        self.arguments = arguments

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        args = ", ".join(map(lambda arg: arg.string(), self.arguments))
        return f"{self.function.string()}({args})"

    def __repr__(self) -> str:
        return f"CallExpression({self.string()})"


class StringLiteral(Expression):
    """
    A string literal.

    ```
    "foobar"
    ```
    """

    def __init__(self, token, value):
        self.token = token
        self.value = value

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return self.token.literal

    def __repr__(self) -> str:
        return f"StringLiteral({self.string()})"


class ArrayLiteral(Expression):
    """
    An array literal. Consists of a sequence of comma-separated expressions.

    ```
    [x, y, z]
    ```
    """

    def __init__(self, token, elements):
        self.token = token
        self.elements = elements

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        elements = ", ".join(map(lambda element: element.string(), self.elements))
        return f"[{elements}]"

    def __repr__(self) -> str:
        return f"ArrayLiteral({self.string()})"


class IndexExpression(Expression):
    """
    An index expression. Consists of an expression (which should evaluate
    to an array), followed by another one (which should evaluate to an
    integer).

    ```
    x[y]
    ```
    """

    def __init__(self, token, left, index):
        self.token = token
        self.left = left
        self.index = index

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        return f"({self.left.string()}[{self.index.string()}])"

    def __repr__(self) -> str:
        return f"IndexExpression({self.string()})"


class HashLiteral(Expression):
    """
    A hash literal. Consists of a collection of key-value pairs.
    More specifically, it consists of a sequence of expression pairs,
    where each pair is separated by a colon and delimited by a comma.

    ```
    {x: y, z: w}
    ```
    """

    def __init__(self, token, pairs):
        self.token = token
        self.pairs = pairs

    @property
    def token_literal(self) -> str:
        return self.token.literal

    def string(self) -> str:
        pairs = ", ".join(f"{k}:{v}" for k, v in self.pairs.items())
        return f"{{{pairs}}}"

    def __repr__(self) -> str:
        return f"HashLiteral({self.string()})"
