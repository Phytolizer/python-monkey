__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

from enum import Enum, auto

from monkey.ast import (
    ArrayLiteral,
    BlockStatement,
    Boolean,
    CallExpression,
    ExpressionStatement,
    FunctionLiteral,
    HashLiteral,
    Identifier,
    IfExpression,
    IndexExpression,
    InfixExpression,
    IntegerLiteral,
    LetStatement,
    PrefixExpression,
    Program,
    ReturnStatement,
    StringLiteral,
)
from monkey.token import TokenKind


class Precedence(Enum):
    """
    The precedence of an infix operator. Used for control flow
    in `Parser._parse_expression`.
    """

    LOWEST = auto()
    EQUALS = auto()
    LESSGREATER = auto()
    SUM = auto()
    PRODUCT = auto()
    PREFIX = auto()
    CALL = auto()
    INDEX = auto()

    def __lt__(self, other):
        return self.value < other.value


_PRECEDENCES = {
    TokenKind.EQ: Precedence.EQUALS,
    TokenKind.NOT_EQ: Precedence.EQUALS,
    TokenKind.LT: Precedence.LESSGREATER,
    TokenKind.GT: Precedence.LESSGREATER,
    TokenKind.PLUS: Precedence.SUM,
    TokenKind.MINUS: Precedence.SUM,
    TokenKind.ASTERISK: Precedence.PRODUCT,
    TokenKind.SLASH: Precedence.PRODUCT,
    TokenKind.LPAREN: Precedence.CALL,
    TokenKind.LBRACKET: Precedence.INDEX,
}


class Parser:
    """
    The Monkey parser. Converts a token stream to a parse tree.
    Requires a `Lexer` at construction time to provide the tokens.
    """

    def __init__(self, lexer):
        self._lexer = lexer
        self._peek_token = None
        self._cur_token = None
        self._errors = []
        self._prefix_parse_fns = {
            TokenKind.IDENT: lambda: self._parse_identifier(),
            TokenKind.INT: lambda: self._parse_integer_literal(),
            TokenKind.BANG: lambda: self._parse_prefix_expression(),
            TokenKind.MINUS: lambda: self._parse_prefix_expression(),
            TokenKind.TRUE: lambda: self._parse_boolean(),
            TokenKind.FALSE: lambda: self._parse_boolean(),
            TokenKind.LPAREN: lambda: self._parse_grouped_expression(),
            TokenKind.IF: lambda: self._parse_if_expression(),
            TokenKind.FUNCTION: lambda: self._parse_function_literal(),
            TokenKind.STRING: lambda: self._parse_string_literal(),
            TokenKind.LBRACKET: lambda: self._parse_array_literal(),
            TokenKind.LBRACE: lambda: self._parse_hash_literal(),
        }
        self._infix_parse_fns = {
            TokenKind.PLUS: lambda left: self._parse_infix_expression(left),
            TokenKind.MINUS: lambda left: self._parse_infix_expression(left),
            TokenKind.ASTERISK: lambda left: self._parse_infix_expression(left),
            TokenKind.SLASH: lambda left: self._parse_infix_expression(left),
            TokenKind.EQ: lambda left: self._parse_infix_expression(left),
            TokenKind.NOT_EQ: lambda left: self._parse_infix_expression(left),
            TokenKind.LT: lambda left: self._parse_infix_expression(left),
            TokenKind.GT: lambda left: self._parse_infix_expression(left),
            TokenKind.LPAREN: lambda left: self._parse_call_expression(left),
            TokenKind.LBRACKET: lambda left: self._parse_index_expression(left),
        }
        self._next_token()
        self._next_token()

    def _next_token(self):
        self._cur_token = self._peek_token
        self._peek_token = self._lexer.next_token()

    def parse_program(self):
        """
        Entry point parsing routine.
        Attempts to take a sequence of Monkey statements and
        convert them to a parse tree.
        """
        statements = []

        while self._cur_token.kind != TokenKind.EOF:
            stmt = self._parse_statement()
            if stmt is not None:
                statements.append(stmt)
            self._next_token()

        return Program(tuple(statements))

    @property
    def errors(self):
        return tuple(self._errors)

    def _parse_statement(self):
        """
        Dispatches on _cur_token.
        """
        if self._cur_token.kind == TokenKind.LET:
            return self._parse_let_statement()
        if self._cur_token.kind == TokenKind.RETURN:
            return self._parse_return_statement()
        return self._parse_expression_statement()

    def _parse_let_statement(self):
        token = self._cur_token

        if not self._expect_peek(TokenKind.IDENT):
            return None

        name = Identifier(self._cur_token, self._cur_token.literal)

        if not self._expect_peek(TokenKind.ASSIGN):
            return None

        self._next_token()

        value = self._parse_expression(Precedence.LOWEST)

        if self._peek_token_is(TokenKind.SEMICOLON):
            self._next_token()

        return LetStatement(token, name, value)

    def _parse_return_statement(self):
        token = self._cur_token

        self._next_token()

        return_value = self._parse_expression(Precedence.LOWEST)

        if self._peek_token_is(TokenKind.SEMICOLON):
            self._next_token()

        return ReturnStatement(token, return_value)

    def _cur_token_is(self, kind):
        return self._cur_token.kind == kind

    def _peek_token_is(self, kind):
        return self._peek_token.kind == kind

    def _cur_precedence(self):
        return _PRECEDENCES.get(self._cur_token.kind, Precedence.LOWEST)

    def _peek_precedence(self):
        return _PRECEDENCES.get(self._peek_token.kind, Precedence.LOWEST)

    def _expect_peek(self, kind):
        if self._peek_token_is(kind):
            self._next_token()
            return True

        self._peek_error(kind)
        return False

    def _peek_error(self, kind):
        msg = f"expected next token to be {kind}, got {self._peek_token.kind} instead"
        self._errors.append(msg)

    def _no_prefix_parse_fn_error(self, kind):
        msg = f"no prefix parse function for {kind} found"
        self._errors.append(msg)

    def _parse_expression_statement(self):
        token = self._cur_token

        expression = self._parse_expression(Precedence.LOWEST)

        if self._peek_token_is(TokenKind.SEMICOLON):
            self._next_token()

        return ExpressionStatement(token, expression)

    def _parse_expression(self, precedence):
        """
        Dispatches on _cur_token.
        """
        prefix = self._prefix_parse_fns.get(self._cur_token.kind)
        if prefix is None:
            self._no_prefix_parse_fn_error(self._cur_token.kind)
            return None
        left_exp = prefix()

        while (
            not self._peek_token_is(TokenKind.SEMICOLON)
            and precedence < self._peek_precedence()
        ):
            infix = self._infix_parse_fns.get(self._peek_token.kind)
            if infix is None:
                return left_exp

            self._next_token()

            left_exp = infix(left_exp)

        return left_exp

    def _parse_identifier(self):
        return Identifier(self._cur_token, self._cur_token.literal)

    def _parse_integer_literal(self):
        token = self._cur_token

        try:
            value = int(self._cur_token.literal)
        except ValueError:
            msg = f'could not parse "{self._cur_token.literal}" as integer'
            self._errors.append(msg)
            return None

        return IntegerLiteral(token, value)

    def _parse_prefix_expression(self):
        token = self._cur_token
        operator = self._cur_token.literal

        self._next_token()

        right = self._parse_expression(Precedence.PREFIX)

        return PrefixExpression(token, operator, right)

    def _parse_infix_expression(self, left):
        token = self._cur_token
        operator = self._cur_token.literal

        precedence = self._cur_precedence()
        self._next_token()
        right = self._parse_expression(precedence)

        return InfixExpression(token, left, operator, right)

    def _parse_boolean(self):
        token = self._cur_token
        value = self._cur_token_is(TokenKind.TRUE)
        return Boolean(token, value)

    def _parse_grouped_expression(self):
        self._next_token()

        exp = self._parse_expression(Precedence.LOWEST)

        if not self._expect_peek(TokenKind.RPAREN):
            return None

        return exp

    def _parse_if_expression(self):
        token = self._cur_token

        if not self._expect_peek(TokenKind.LPAREN):
            return None

        self._next_token()

        condition = self._parse_expression(Precedence.LOWEST)

        if not self._expect_peek(TokenKind.RPAREN):
            return None

        if not self._expect_peek(TokenKind.LBRACE):
            return None

        consequence = self._parse_block_statement()

        if self._peek_token_is(TokenKind.ELSE):
            self._next_token()

            if not self._expect_peek(TokenKind.LBRACE):
                return None

            alternative = self._parse_block_statement()
        else:
            alternative = None

        return IfExpression(token, condition, consequence, alternative)

    def _parse_block_statement(self):
        token = self._cur_token
        statements = []

        self._next_token()

        while self._cur_token.kind not in (TokenKind.RBRACE, TokenKind.EOF):
            stmt = self._parse_statement()
            if stmt is not None:
                statements.append(stmt)
            self._next_token()

        return BlockStatement(token, tuple(statements))

    def _parse_function_literal(self):
        token = self._cur_token

        if not self._expect_peek(TokenKind.LPAREN):
            return None

        parameters = self._parse_function_parameters()

        if not self._expect_peek(TokenKind.LBRACE):
            return None

        body = self._parse_block_statement()

        return FunctionLiteral(token, parameters, body)

    def _parse_function_parameters(self):
        parameters = []

        if self._peek_token_is(TokenKind.RPAREN):
            self._next_token()
            return tuple(parameters)

        self._next_token()

        parameters.append(self._parse_identifier())

        while self._peek_token_is(TokenKind.COMMA):
            self._next_token()
            self._next_token()
            parameters.append(self._parse_identifier())

        if not self._expect_peek(TokenKind.RPAREN):
            return None

        return tuple(parameters)

    def _parse_call_expression(self, function):
        token = self._cur_token

        arguments = self._parse_expression_list(TokenKind.RPAREN)

        return CallExpression(token, function, arguments)

    def _parse_string_literal(self):
        return StringLiteral(self._cur_token, self._cur_token.literal)

    def _parse_array_literal(self):
        token = self._cur_token

        elements = self._parse_expression_list(TokenKind.RBRACKET)

        return ArrayLiteral(token, elements)

    def _parse_expression_list(self, end):
        result = []

        if self._peek_token_is(end):
            self._next_token()
            return tuple(result)

        self._next_token()
        result.append(self._parse_expression(Precedence.LOWEST))

        while self._peek_token_is(TokenKind.COMMA):
            self._next_token()
            self._next_token()
            result.append(self._parse_expression(Precedence.LOWEST))

        if not self._expect_peek(end):
            return None

        return tuple(result)

    def _parse_index_expression(self, left):
        token = self._cur_token

        self._next_token()

        index = self._parse_expression(Precedence.LOWEST)

        if not self._expect_peek(TokenKind.RBRACKET):
            return None

        return IndexExpression(token, left, index)

    def _parse_hash_literal(self):
        token = self._cur_token
        pairs = {}

        while not self._peek_token_is(TokenKind.RBRACE):
            self._next_token()
            key = self._parse_expression(Precedence.LOWEST)

            if not self._expect_peek(TokenKind.COLON):
                return None

            self._next_token()
            value = self._parse_expression(Precedence.LOWEST)

            pairs[key] = value

            if not self._peek_token_is(TokenKind.RBRACE) and not self._expect_peek(
                TokenKind.COMMA
            ):
                return None

        if not self._expect_peek(TokenKind.RBRACE):
            return None

        return HashLiteral(token, pairs)
