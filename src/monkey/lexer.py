"""
Definition of the Lexer class.
"""
__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

from monkey.token import Token, TokenKind

_KEYWORDS = {
    "else": TokenKind.ELSE,
    "false": TokenKind.FALSE,
    "fn": TokenKind.FUNCTION,
    "if": TokenKind.IF,
    "let": TokenKind.LET,
    "return": TokenKind.RETURN,
    "true": TokenKind.TRUE,
}


def _is_digit(ch):
    return ch >= "0" and ch <= "9"


def _is_letter(ch):
    return (ch >= "a" and ch <= "z") or (ch >= "A" and ch <= "Z") or ch == "_"


def _is_space(ch):
    return ch in (" ", "\t", "\n", "\r")


class Lexer:
    """
    The Monkey lexer.

    The lexer takes the entire input in its constructor.
    This input can be converted to a sequence of `Token` objects
    by repeatedly calling `next_token()`.

    End of file is signalled by the appearance of a
    token with kind `TokenKind.EOF`.
    """

    def __init__(self, text):
        self._text = text
        self._position = 0
        self._read_position = 0
        self._ch = "\0"

        self._read_char()

    def _read_char(self):
        self._ch = self._peek_char()
        self._position = self._read_position
        self._read_position += 1

    def _peek_char(self):
        if self._read_position >= len(self._text):
            return "\0"
        else:
            return self._text[self._read_position]

    def next_token(self):
        """
        Receive the next token from the input text.
        If there is no next token, the returned token's kind is `TokenKind.EOF`.
        """
        self._skip_whitespace()

        if _is_letter(self._ch):
            tok = Token(TokenKind.IDENT, self._read_identifier())
            tok.kind = _KEYWORDS.get(tok.literal, TokenKind.IDENT)
            return tok
        elif _is_digit(self._ch):
            tok = Token(TokenKind.INT, self._read_number())
            return tok
        elif self._ch == '"':
            tok = Token(TokenKind.STRING, self._read_string())
        elif self._ch == "=" and self._peek_char() == "=":
            ch = self._ch
            self._read_char()
            tok = Token(TokenKind.EQ, f"{ch}{self._ch}")
        elif self._ch == "!" and self._peek_char() == "=":
            ch = self._ch
            self._read_char()
            tok = Token(TokenKind.NOT_EQ, f"{ch}{self._ch}")
        elif self._ch == "=":
            tok = Token(TokenKind.ASSIGN, self._ch)
        elif self._ch == "!":
            tok = Token(TokenKind.BANG, self._ch)
        elif self._ch == "-":
            tok = Token(TokenKind.MINUS, self._ch)
        elif self._ch == "/":
            tok = Token(TokenKind.SLASH, self._ch)
        elif self._ch == "*":
            tok = Token(TokenKind.ASTERISK, self._ch)
        elif self._ch == "<":
            tok = Token(TokenKind.LT, self._ch)
        elif self._ch == ">":
            tok = Token(TokenKind.GT, self._ch)
        elif self._ch == ";":
            tok = Token(TokenKind.SEMICOLON, self._ch)
        elif self._ch == ":":
            tok = Token(TokenKind.COLON, self._ch)
        elif self._ch == "(":
            tok = Token(TokenKind.LPAREN, self._ch)
        elif self._ch == ")":
            tok = Token(TokenKind.RPAREN, self._ch)
        elif self._ch == "{":
            tok = Token(TokenKind.LBRACE, self._ch)
        elif self._ch == "}":
            tok = Token(TokenKind.RBRACE, self._ch)
        elif self._ch == "[":
            tok = Token(TokenKind.LBRACKET, self._ch)
        elif self._ch == "]":
            tok = Token(TokenKind.RBRACKET, self._ch)
        elif self._ch == ",":
            tok = Token(TokenKind.COMMA, self._ch)
        elif self._ch == "+":
            tok = Token(TokenKind.PLUS, self._ch)
        elif self._ch == "\0":
            tok = Token(TokenKind.EOF, "")
        else:
            tok = Token(TokenKind.ILLEGAL, self._ch)

        self._read_char()
        return tok

    def _read_identifier(self):
        position = self._position
        while _is_letter(self._ch):
            self._read_char()
        return self._text[position : self._position]

    def _read_number(self):
        position = self._position
        while _is_digit(self._ch):
            self._read_char()
        return self._text[position : self._position]

    def _skip_whitespace(self):
        while _is_space(self._ch):
            self._read_char()

    def _read_string(self):
        position = self._position + 1
        while True:
            self._read_char()
            if self._ch in ('"', "\0"):
                break
        return self._text[position : self._position]
