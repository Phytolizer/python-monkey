import monkey
from monkey.ast import (
    ArrayLiteral,
    BlockStatement,
    CallExpression,
    ExpressionStatement,
    FunctionLiteral,
    HashLiteral,
    Identifier,
    IfExpression,
    IndexExpression,
    InfixExpression,
    IntegerLiteral,
    LetStatement,
    PrefixExpression,
    Program,
    ReturnStatement,
    StringLiteral,
)
from monkey.object import (
    Array,
    Builtin,
    Environment,
    Error,
    Function,
    Hash,
    Hashable,
    HashPair,
    Integer,
    Null,
    ObjectType,
    ReturnValue,
    String,
)


def _monkey_len(args):
    if len(args) != 1:
        return Error(f"wrong number of arguments. got={len(args)}, want=1")

    arg = args[0]
    if isinstance(arg, String):
        return Integer(len(arg.value))
    if isinstance(arg, Array):
        return Integer(len(arg.elements))
    return Error(f"argument to `len` not supported, got {arg.type}")


def _monkey_first(args):
    if len(args) != 1:
        return Error(f"wrong number of arguments. got={len(args)}, want=1")
    if args[0].type != ObjectType.ARRAY:
        return Error(f"argument to `first` must be ARRAY, got {args[0].type}")
    arr = args[0]
    if len(arr.elements) > 0:
        return arr.elements[0]
    return _NULL


def _monkey_last(args):
    if len(args) != 1:
        return Error(f"wrong number of arguments. got={len(args)}, want=1")
    if args[0].type != ObjectType.ARRAY:
        return Error(f"argument to `last` must be ARRAY, got {args[0].type}")
    arr = args[0]
    if len(arr.elements) > 0:
        return arr.elements[-1]
    return _NULL


def _monkey_rest(args):
    if len(args) != 1:
        return Error(f"wrong number of arguments. got={len(args)}, want=1")
    if args[0].type != ObjectType.ARRAY:
        return Error(f"argument to `rest` must be ARRAY, got {args[0].type}")
    arr = args[0]
    if len(arr.elements) > 0:
        return Array(arr.elements[1:])
    return _NULL


def _monkey_push(args):
    if len(args) != 2:
        return Error(f"wrong number of arguments. got={len(args)}, want=2")
    if args[0].type != ObjectType.ARRAY:
        return Error(f"argument to `push` must be ARRAY, got {args[0].type}")
    arr = args[0]
    result = []
    result.extend(arr.elements)
    result.append(args[1])
    return Array(result)


def _monkey_puts(args):
    for arg in args:
        print(arg.inspect())

    return _NULL


_NULL = Null()
TRUE = monkey.object.Boolean(True)
FALSE = monkey.object.Boolean(False)
_BUILTINS = {
    "len": Builtin(_monkey_len),
    "first": Builtin(_monkey_first),
    "last": Builtin(_monkey_last),
    "rest": Builtin(_monkey_rest),
    "push": Builtin(_monkey_push),
    "puts": Builtin(_monkey_puts),
}


def _native_bool_to_boolean_object(value):
    return TRUE if value else FALSE


def _is_truthy(obj):
    if obj in (_NULL, FALSE):
        return False
    return True


def _is_error(obj):
    return obj is not None and obj.type == ObjectType.ERROR


class Evaluator:
    def eval(self, node, env):
        # Statements
        if isinstance(node, Program):
            return self._eval_program(node, env)
        if isinstance(node, ExpressionStatement):
            return self.eval(node.expression, env)
        if isinstance(node, BlockStatement):
            return self._eval_block_statement(node, env)
        if isinstance(node, ReturnStatement):
            val = self.eval(node.return_value, env)
            if _is_error(val):
                return val
            return ReturnValue(val)
        if isinstance(node, LetStatement):
            val = self.eval(node.value, env)
            if _is_error(val):
                return val
            env.set(node.name.value, val)

        # Expressions
        if isinstance(node, FunctionLiteral):
            params = node.parameters
            body = node.body
            return Function(params, body, env)
        if isinstance(node, CallExpression):
            function = self.eval(node.function, env)
            if _is_error(function):
                return function
            args = self._eval_expressions(node.arguments, env)
            if len(args) == 1 and _is_error(args[0]):
                return args[0]
            return self._apply_function(function, args)
        if isinstance(node, IfExpression):
            return self._eval_if_expression(node, env)
        if isinstance(node, InfixExpression):
            left = self.eval(node.left, env)
            if _is_error(left):
                return left
            right = self.eval(node.right, env)
            if _is_error(right):
                return right
            return self._eval_infix_expression(node.operator, left, right)
        if isinstance(node, PrefixExpression):
            right = self.eval(node.right, env)
            if _is_error(right):
                return right
            return self._eval_prefix_expression(node.operator, right)
        if isinstance(node, IndexExpression):
            left = self.eval(node.left, env)
            if _is_error(left):
                return left
            index = self.eval(node.index, env)
            if _is_error(index):
                return index
            return self._eval_index_expression(left, index)
        if isinstance(node, HashLiteral):
            return self._eval_hash_literal(node, env)
        if isinstance(node, ArrayLiteral):
            elements = self._eval_expressions(node.elements, env)
            if len(elements) == 1 and _is_error(elements[0]):
                return elements[0]
            return Array(elements)
        if isinstance(node, StringLiteral):
            return String(node.value)
        if isinstance(node, Identifier):
            try:
                val = env.get(node.value)
            except KeyError:
                val = _BUILTINS.get(node.value)
                if val is not None:
                    return val
                return Error(f"identifier not found: {node.value}")
            else:
                return val
        if isinstance(node, IntegerLiteral):
            return Integer(node.value)
        if isinstance(node, monkey.ast.Boolean):
            return _native_bool_to_boolean_object(node.value)
        return None

    def _eval_program(self, program, env):
        result = None

        for statement in program.statements:
            result = self.eval(statement, env)

            if isinstance(result, ReturnValue):
                return result.value
            if isinstance(result, Error):
                return result

        return result

    def _eval_block_statement(self, block, env):
        result = None

        for statement in block.statements:
            result = self.eval(statement, env)

            if result is not None and result.type in (
                ObjectType.RETURN_VALUE,
                ObjectType.ERROR,
            ):
                return result

        return result

    def _eval_prefix_expression(self, operator, right):
        if operator == "!":
            return self._eval_bang_operator_expression(right)
        if operator == "-":
            return self._eval_minus_prefix_operator_expression(right)
        return Error(f"unknown operator: {operator}{right.type}")

    def _eval_index_expression(self, left, index):
        if left.type == ObjectType.ARRAY and index.type == ObjectType.INTEGER:
            return self._eval_array_index_expression(left, index)
        if left.type == ObjectType.HASH:
            return self._eval_hash_index_expression(left, index)
        return Error(f"index operator not supported: {left.type}")

    def _eval_hash_index_expression(self, hash, index):
        if not isinstance(index, Hashable):
            return Error(f"unusable as hash key: {index.type}")

        pair = hash.pairs.get(index.hash_key())
        if pair is None:
            return _NULL

        return pair.value

    def _eval_array_index_expression(self, array, index):
        idx = index.value
        max = len(array.elements) - 1

        if idx < 0 or idx > max:
            return _NULL

        return array.elements[idx]

    def _eval_bang_operator_expression(self, right):
        if right in (FALSE, _NULL):
            return TRUE
        return FALSE

    def _eval_minus_prefix_operator_expression(self, right):
        if right.type != ObjectType.INTEGER:
            return Error(f"unknown operator: -{right.type}")

        value = right.value
        return Integer(-value)

    def _eval_infix_expression(self, operator, left, right):
        if left.type == ObjectType.INTEGER and right.type == ObjectType.INTEGER:
            return self._eval_integer_infix_expression(operator, left, right)
        if left.type == ObjectType.STRING and right.type == ObjectType.STRING:
            return self._eval_string_infix_expression(operator, left, right)
        if operator == "==":
            return _native_bool_to_boolean_object(left == right)
        if operator == "!=":
            return _native_bool_to_boolean_object(left != right)
        if left.type != right.type:
            return Error(f"type mismatch: {left.type} {operator} {right.type}")
        return Error(f"unknown operator: {left.type} {operator} {right.type}")

    def _eval_integer_infix_expression(self, operator, left, right):
        left_val = left.value
        right_val = right.value

        if operator == "+":
            return Integer(left_val + right_val)
        if operator == "-":
            return Integer(left_val - right_val)
        if operator == "*":
            return Integer(left_val * right_val)
        if operator == "/":
            return Integer(left_val // right_val)
        if operator == "<":
            return _native_bool_to_boolean_object(left_val < right_val)
        if operator == ">":
            return _native_bool_to_boolean_object(left_val > right_val)
        if operator == "==":
            return _native_bool_to_boolean_object(left_val == right_val)
        if operator == "!=":
            return _native_bool_to_boolean_object(left_val != right_val)
        return Error(f"unknown operator: {left.type} {operator} {right.type}")

    def _eval_string_infix_expression(self, operator, left, right):
        if operator != "+":
            return Error(f"unknown operator: {left.type} {operator} {right.type}")

        left_val = left.value
        right_val = right.value
        return String(left_val + right_val)

    def _eval_if_expression(self, node, env):
        condition = self.eval(node.condition, env)
        if _is_error(condition):
            return condition

        if _is_truthy(condition):
            return self.eval(node.consequence, env)
        if node.alternative is not None:
            return self.eval(node.alternative, env)
        return _NULL

    def _eval_expressions(self, exps, env):
        result = []
        for e in exps:
            evaluated = self.eval(e, env)
            if _is_error(evaluated):
                return (evaluated,)
            result.append(evaluated)
        return tuple(result)

    def _apply_function(self, function, args):
        if isinstance(function, Function):
            extended_env = self._extend_function_env(function, args)
            evaluated = self.eval(function.body, extended_env)
            return self._unwrap_return_value(evaluated)
        if isinstance(function, Builtin):
            return function.fn(args)
        return Error(f"not a function: {function.type}")

    def _extend_function_env(self, function, args):
        env = Environment(function.env)
        for (param, arg) in zip(function.parameters, args):
            env.set(param.value, arg)
        return env

    def _unwrap_return_value(self, obj):
        if isinstance(obj, ReturnValue):
            return obj.value
        return obj

    def _eval_hash_literal(self, node, env):
        pairs = {}

        for (key_node, value_node) in node.pairs.items():
            key = self.eval(key_node, env)
            if _is_error(key):
                return key

            if not isinstance(key, Hashable):
                return Error(f"unusable as hash key: {key.type}")

            value = self.eval(value_node, env)
            if _is_error(value):
                return value

            hashed = key.hash_key()
            pairs[hashed] = HashPair(key, value)

        return Hash(pairs)
