__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

from enum import Enum


class TokenKind(str, Enum):
    """
    Classifies tokens returned by the lexer.
    The string values are a user-friendly representation of the token.
    """

    ILLEGAL = "ILLEGAL"
    EOF = "EOF"

    IDENT = "IDENT"
    INT = "INT"
    STRING = "STRING"

    ASSIGN = "="
    PLUS = "+"
    MINUS = "-"
    BANG = "!"
    ASTERISK = "*"
    SLASH = "/"

    LT = "<"
    GT = ">"

    EQ = "=="
    NOT_EQ = "!="

    COMMA = ","
    SEMICOLON = ";"
    COLON = ":"

    LPAREN = "("
    RPAREN = ")"
    LBRACE = "{"
    RBRACE = "}"
    LBRACKET = "["
    RBRACKET = "]"

    ELSE = "else"
    FALSE = "false"
    FUNCTION = "fn"
    IF = "if"
    LET = "let"
    RETURN = "return"
    TRUE = "true"


class Token:
    """
    The unit of Monkey.

    Has two properties:
    * `kind` -- the kind of token this is.
    * `literal` -- the literal text of this token.
    """

    def __init__(self, kind, literal):
        self.kind = kind
        self.literal = literal

    def __str__(self):
        return f"Token({self.kind}, {self.literal})"
