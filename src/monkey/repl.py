__author__ = "Phytolizer"
__copyright__ = "Phytolizer"
__license__ = "MIT"

from monkey.evaluator import Evaluator
from monkey.lexer import Lexer
from monkey.object import Environment
from monkey.parser import Parser

PROMPT = ">> "


def _print_parser_errors(out_stream, errors):
    for error in errors:
        print(f"\t{error}", file=out_stream)


def start(in_stream, out_stream):
    env = Environment()
    while True:
        print(PROMPT, file=out_stream, end="")
        out_stream.flush()
        line = in_stream.readline()
        if len(line) == 0:
            break

        lexer = Lexer(line)
        parser = Parser(lexer)
        program = parser.parse_program()

        if len(parser.errors) > 0:
            _print_parser_errors(out_stream, parser.errors)
        else:
            evaluated = Evaluator().eval(program, env)
            if evaluated is not None:
                print(evaluated.inspect(), file=out_stream)
